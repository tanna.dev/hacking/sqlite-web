package main

import (
	"database/sql"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"regexp"

	_ "modernc.org/sqlite"
)

type Todo struct {
	Title string
	Done  bool
}

type TodoPageData struct {
	PageTitle string
	Todos     []Todo
	SQL       string
	Rows      [][]any
}

// adapted via Datasette TODO link + attribution
var reCommentString = regexp.MustCompile(
	// Start of string, then any amount of whitespace
	"^\\s*(" +
		// Comment that starts with -- and ends at a newline
		"(?:\\-\\-.*?\\n\\s*)" +
		// # Comment that starts with /* and ends with */ - but does not have */ in it
		"|(?:\\/\\*((?!\\*\\/)[\\s\\S])*\\*\\/)" +
		// # Whitespace
		"\\s*)*\\s*",
)

func main() {
	db, err := sql.Open("sqlite", "./dmd.db")
	must(err)

	// rows, err := db.Query("SELECT * FROM renovate LIMIT 5")
	// must(err)
	// fmt.Printf("rows: %v\n", rows)

	tmpl := template.Must(template.ParseFiles("layout.html"))
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		data := TodoPageData{
			PageTitle: "My TODO list",
			Todos: []Todo{
				{Title: "Task 1", Done: false},
				{Title: "Task 2", Done: true},
				{Title: "Task 3", Done: true},
			},
		}
		must(tmpl.Execute(w, data))
	})
	http.HandleFunc("/sql", func(w http.ResponseWriter, r *http.Request) {
		// must(err)

		data := TodoPageData{
			PageTitle: "My TODO list",
			Todos: []Todo{
				{Title: "Task 1", Done: false},
				{Title: "Task 2", Done: true},
				{Title: "Task 3", Done: true},
			},
			SQL: r.URL.Query().Get("sql"),
		}
		rows, err := db.Query(data.SQL)
		if httpMust(err, w) {
			return
		}

		cols, err := rows.Columns()
		if httpMust(err, w) {
			return
		}
		colTypes, err := rows.ColumnTypes()
		if httpMust(err, w) {
			return
		}
		fmt.Printf("colTypes: %v\n", colTypes)

		for k, v := range colTypes {
			fmt.Printf("k: %v\n", k)
			fmt.Printf("v: %v\n", v)
		}

		var allRows [][]any
		for rows.Next() {
			// via https://stackoverflow.com/a/62333455/2257038
			row := make([]any, len(cols))
			for i := range cols {
				row[i] = new(any)
			}

			err = rows.Scan(row...)
			if httpMust(err, w) {
				return
			}

			allRows = append(allRows, row)
		}

		fmt.Printf("allRows: %v\n", allRows)

		data.Rows = allRows

		must(tmpl.Execute(w, data))
	})
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))

	log.Fatal(http.ListenAndServe(":8080", nil))

}

func must(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func httpMust(err error, w http.ResponseWriter) bool {
	if err == nil {
		return false
	}

	w.WriteHeader(http.StatusInternalServerError)
	w.Write([]byte(err.Error()))

	return true
}
